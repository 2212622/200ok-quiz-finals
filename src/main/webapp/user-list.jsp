<!--
* Provides the main user interface for managing users. It shows all users and
includes buttons for, editing, adding, and deleting user information.
* Author: Ramesh Fadatare
* Source: https://www.javaguides.net/2019/03/jsp-servlet-jdbc-mysql-crud-example-tutorial.html
* Edited by: Alfredo R. Mangilinan III
*/
-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>User Management Application</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: #ffccdc">
			<div class="navbar-brand" style="color: #444"> <strong> User Management App </strong> </div>
			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link" style="color: #444">Users</a></li>
			</ul>
		</nav>
	</header>
	<br>

	<div class="row">
		<!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

		<div class="container">
			<h3 class="text-center" style="color: #444">List of Users</h3>
			<hr>
			<div class="container text-left">
				<a href="<%=request.getContextPath()%>/new" class="btn btn-success"
				   style="
				 	background-color: #F8C8DC;
				 	border: none;
				 	color: #fff;
					text-transform: uppercase;
					font-weight: 600;
					margin: 10px 0;
					cursor: pointer;">
					Add New User</a>
				<form action="search" method="post">
					<fieldset class="form-group">
						<label>Search User</label> <input type="text" style= "width: 30%" class="form-control" name="search"  maxlength="25">
					</fieldset>
					<button type="submit" class="btn btn-success">Search</button>
				</form>

			</div>
			<br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Username</th>
						<th>Password</th>
						<th>Role</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<!--   for (Todo todo: todos) {  -->
					<c:forEach var="user" items="${listUser}">
						<tr>
							<td><c:out value="${user.ID}" /></td>
							<td><c:out value="${user.username}" /></td>
							<td> <c:out value="${user.password}"/></td>
							<td><c:out value="${user.role}" /></td>
							<td><a href="edit?id=<c:out value='${user.ID}' />">Edit</a>
								&nbsp;&nbsp;&nbsp;&nbsp; <a
								href="delete?id=<c:out value='${user.ID}' />">Delete</a></td>
						</tr>
					</c:forEach>
					<!-- } -->
				</tbody>

			</table>
		</div>
	</div>
</body>
</html>
