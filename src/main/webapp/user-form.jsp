<!--
* Provides user interface for adding a new user and editing user information.
* Author: Ramesh Fadatare
* Source: https://www.javaguides.net/2019/03/jsp-servlet-jdbc-mysql-crud-example-tutorial.html
* Edited by: Alfredo R. Mangilinan III
*/
-->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			 style="background-color: #ff007f">
			<div class="navbar-brand" style="color: #FFEDE7"> <strong> User Management App </strong> </div>
			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					   class="nav-link" style="color: #FFEDE7">Users</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="container col-md-5">
		<div class="card">
			<div class="card-body">
				<c:if test="${user != null}">
					<form action="update" method="post">
				</c:if>
				<c:if test="${user == null}">
					<form action="insert" method="post">
				</c:if>

				<caption>
					<h2>
						<c:if test="${user != null}">
            			Edit User
            		</c:if>
						<c:if test="${user == null}">
            			Add New User
            		</c:if>
					</h2>
				</caption>

				<c:if test="${user == null}">
					<label>User ID</label>
					<fieldset class="form-group">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="student" value="student" />
							<label class="form-check-label" for="student">Student</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="teacher" value="teacher" />
							<label class="form-check-label" for="teacher">Teacher</label>
						</div>
						<input type="text" value="<c:out value='${user.ID}' />" class="form-control" name="ID" title="Up to 7 digits" maxlength="7">
					</fieldset>
				</c:if>
					<input type="hidden" name="ID" value="<c:out value='${user.ID}' />" />
				<fieldset class="form-group">
					<label>User Name</label> <input type="text"
						value="<c:out value='${user.username}' />" class="form-control"
						name="username" required="required" title= "No special characters, less than 25 characters" maxlength="25">
				</fieldset>

				<fieldset class="form-group">
					<label>User Password</label> <input type="text"
						value="<c:out value='${user.password}' />" class="form-control"
						name="password" title="Not more than 16 characters, no white spaces" maxlength="16">
				</fieldset>
					<p> <strong> Whitespaces and empty input fields are not allowed.</strong></p>
				<button type="submit" class="btn btn-success">Save</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
