package com.module.adminmodule;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * This  Data Access Layer (DAO) class provides CRUD database operations for the tables in the database.
 * @author Ramesh Fadatare
 * Source: https://www.javaguides.net/2019/03/jsp-servlet-jdbc-mysql-crud-example-tutorial.html
 * Edited by: Alfredo R. Mangilinan III
 */
public class UserDAO {
	private String jdbcURL = "jdbc:mysql://localhost:3306/200okquiz";
	private String jdbcUsername = "root";
	private String jdbcPassword = "";
	private static final String INSERT_STUDENT = "INSERT INTO students" + "  (studentID, username, password) VALUES "
			+ " (?, ?, ?)";
	private static final String INSERT_TEACHER = "INSERT INTO teachers" + "  (teachersID, password, username) VALUES "
			+ " (?, ?, ?)";
	private static final String SELECT_USER_BY_STUDENTID = "select studentID,username,password from students where studentID = ?";
	private static final String SELECT_USER_BY_TEACHERID = "select teachersID,username,password from teachers where teachersID = ?";
	private static final String SELECT_ALL_STUDENTS = "select studentID, username, password from students";
	private static final String SELECT_ALL_TEACHERS = "select teachersID, username, password from teachers;";
	private static final String DELETE_STUDENT = "delete from students where studentID = ?";
	private static final String DELETE_TEACHER = "delete from teachers where teachersID = ?";
	private static final String UPDATE_STUDENT = "update students set username = ?, password = ? where studentID = ?";
	private static final String UPDATE_TEACHER = "update teachers set password = ?, username = ? where teachersID = ?";
	private static final String CHECK_IF_ID_EXISTS_IN_STUDENTS = "select studentID from students where studentID = ?";
	private static final String CHECK_IF_ID_EXISTS_IN_TEACHERS = "select teachersID from teachers where teachersID = ?";

	public UserDAO() {
	}

	protected Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
			System.out.println("Connected to Database.");
			return connection;
		} catch (SQLException e) {
			throw new RuntimeException("Cannot connect to database", e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Class not found, ",e);
		}
	}

	public void insertUser(User user, String userType) throws SQLException {
		if (userType.equalsIgnoreCase("student")) {
			try (Connection connection = getConnection();
				 PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STUDENT);) {
				preparedStatement.setInt(1, user.getID());
				preparedStatement.setString(2, user.getUsername());
				preparedStatement.setString(3, user.getPassword());
				System.out.println(preparedStatement);
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				printSQLException(e);
			}
		}
		if (userType.equalsIgnoreCase("teacher")) {
			try (Connection connection = getConnection();
				 PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TEACHER);) {
				preparedStatement.setInt(1, user.getID());
				preparedStatement.setString(2, user.getPassword());
				preparedStatement.setString(3, user.getUsername());
				System.out.println(preparedStatement);
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				printSQLException(e);
			}
		}
	}

	public User selectUser(int id) throws SQLException {
		User user = null;
		String userType = getUserType(id);
		try (Connection connection = getConnection()) {
			PreparedStatement statement;
			ResultSet rs;
			if (userType.equalsIgnoreCase("student")) {
				statement = connection.prepareStatement(SELECT_USER_BY_STUDENTID);
				statement.setInt(1, id);
				rs = statement.executeQuery();
				while (rs.next()) {
					int studentID = rs.getInt("studentID");
					String username = rs.getString("username");
					String password = rs.getString("password");
					user = new User(studentID, username, password, userType);
				}
			} else {
				statement = connection.prepareStatement(SELECT_USER_BY_TEACHERID);
				statement.setInt(1, id);
				rs = statement.executeQuery();
				while (rs.next()) {
					int teacherID = rs.getInt("teachersID");
					String username = rs.getString("username");
					String password = rs.getString("password");
					user = new User(teacherID, username, password, userType);
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return user;
	}

	public List<User> selectAllUsers() {
		// using try-with-resources to avoid closing resources (boiler plate code)
		List<User> users = new ArrayList<>();
		// Step 1: Establishing a Connection
		try (Connection connection = getConnection();
			 // Step 2:Create a statement using connection object
			 PreparedStatement preparedStatement1 = connection.prepareStatement(SELECT_ALL_STUDENTS);) {
			System.out.println(preparedStatement1);
			// Step 3: Execute the query or update query
			ResultSet students = preparedStatement1.executeQuery();
			// Step 4: Process the ResultSet object.
			while (students.next()) {
				int studentID = students.getInt("studentID");
				String username = students.getString("username");
				String password = students.getString("password");
				StringBuilder maskedPassword = new StringBuilder();
				char [] pwd = password.toCharArray();
				//convert the password to *, showing the length of the password based on number of *
				for (int index = 0; index < pwd.length; index++) {
					maskedPassword.append("*");
				}
				User userToVerify = new User(studentID, username, password);
				String userType = getUserType(userToVerify);
				users.add(new User(studentID, username, maskedPassword.toString(),userType));
			}

			PreparedStatement preparedStatement2 = connection.prepareStatement(SELECT_ALL_TEACHERS);
			System.out.println(preparedStatement2);
			ResultSet teachers = preparedStatement2.executeQuery();
			while (teachers.next()) {
				int teachersID = teachers.getInt("teachersID");
				String password = teachers.getString("password");
				StringBuilder maskedPassword = new StringBuilder();
				char [] pwd = password.toCharArray();
				//convert the password to *, showing the length of the password based on number of *
				for (int index = 0; index < pwd.length; index++) {
					maskedPassword.append("*");
				}
				String username = teachers.getString("username");
				User userToVerify = new User(teachersID, username, password);
				String userType = getUserType(userToVerify);
				users.add(new User(teachersID, username, maskedPassword.toString(),userType));
			}

		} catch (SQLException e) {
			printSQLException(e);
		}
		return users;
	}

	public boolean deleteUser(int id, String studentType) throws SQLException {
		boolean rowDeleted;
		if (studentType.equalsIgnoreCase("student")) {
			try (Connection connection = getConnection();
				 PreparedStatement statement = connection.prepareStatement(DELETE_STUDENT);) {
				statement.setInt(1, id);
				rowDeleted = statement.executeUpdate() > 0;
		}} else {
			try (Connection connection = getConnection();
				 PreparedStatement statement = connection.prepareStatement(DELETE_TEACHER);) {
				statement.setInt(1, id);
				rowDeleted = statement.executeUpdate() > 0;
			}
		}
		return rowDeleted;
	}

	public boolean updateUser(User user) throws SQLException {
		boolean rowUpdated;
		String studentType = getUserType(user.getID());
		try (Connection connection = getConnection()) {
			PreparedStatement statement;
			if (studentType.equalsIgnoreCase("student")) {
				statement = connection.prepareStatement(UPDATE_STUDENT);
				statement.setString(1, user.getUsername());
				statement.setString(2, user.getPassword());
			} else {
				statement = connection.prepareStatement(UPDATE_TEACHER);
				statement.setString(1, user.getPassword());
				statement.setString(2, user.getUsername());
			}
			statement.setInt(3,user.getID());
			rowUpdated = statement.executeUpdate() > 0;
		}
		return rowUpdated;
	}

	public String getUserType(int id) throws SQLException {
		String studentType = "teacher";
		try (Connection connection = getConnection();
			 PreparedStatement statement = connection.prepareStatement(CHECK_IF_ID_EXISTS_IN_STUDENTS);) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				if (rs.getInt("studentID") == id) {
					studentType = "student";
				}
			}
		}
		return studentType;
	}

	public String getUserType(User user) throws SQLException {
		String userType = "teacher";
		try (Connection connection = getConnection();
			 PreparedStatement statement = connection.prepareStatement(CHECK_IF_ID_EXISTS_IN_STUDENTS);) {
			statement.setInt(1, user.getID());
			ResultSet rs = statement.executeQuery();
			PreparedStatement statement2 = connection.prepareStatement(SELECT_USER_BY_STUDENTID);
			statement2.setInt(1, user.getID());
			ResultSet rs2 = statement2.executeQuery();
			while (rs.next()) {
				if (rs.getInt("studentID") == user.getID()) {
					//access the entries from students table
					while (rs2.next()) {
						//if username and password of the current user id matches an entry in the students table,
						//the current id is indeed a student
						if (rs2.getString("username").equals(user.getUsername())
							&& rs2.getString("password").equals(user.getPassword())) {
							userType = "student";
						} else {
							userType = "teacher";
						}
					}
				}
			}
		}
		return userType;
	}

	public boolean checkIfIDExists (int id, String userType) throws SQLException {
		boolean idExists = false;
		try (Connection connection = getConnection()) {
			switch (userType) {
				case "student":
					PreparedStatement statement = connection.prepareStatement(CHECK_IF_ID_EXISTS_IN_STUDENTS);
					statement.setInt(1, id);
					ResultSet rs = statement.executeQuery();
					while (rs.next()) {
						if (rs.getInt("studentID") == id) {
							idExists = true;
						}
					}
					break;
				case "teacher":
					PreparedStatement statement2 = connection.prepareStatement(CHECK_IF_ID_EXISTS_IN_TEACHERS);
					statement2.setInt(1, id);
					rs = statement2.executeQuery();
					while (rs.next()) {
						if (rs.getInt("teachersID") == id) {
							idExists = true;
						}
					}
					break;
				default:
					break;
			}
		}
		return idExists;
	}

	private void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}
}
