package com.module.adminmodule;

/**
 * This is a model class that represents a User entity
 * @author Ramesh Fadatare
 * Source: https://www.javaguides.net/2019/03/jsp-servlet-jdbc-mysql-crud-example-tutorial.html
 * Edited by: Alfredo R. Mangilinan III
 */
public class User {
	protected int ID;
	protected String username;
	protected String password;

	protected String role;

	public User() {
	}
	
	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public User(int ID, String username, String password, String role) {
		super();
		this.ID = ID;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public User(int ID, String username, String password) {
		super();
		this.ID = ID;
		this.username = username;
		this.password = password;
	}


    public int getID() {
		return ID;
	}
	public void setID(int ID) {
		this.ID = ID;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}
