package com.module.adminmodule;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides capabilities to the web application.
 * @author Ramesh Fadatare
 * Source: https://www.javaguides.net/2019/03/jsp-servlet-jdbc-mysql-crud-example-tutorial.html
 * Edited by: Alfredo R. Mangilinan III
 */
@WebServlet("/")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDAO userDAO;

    public void init() {
        userDAO = new UserDAO();
    }

    //overriden method from the WebServlet annotation
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    //overriden method from the WebServlet annotation
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        System.out.println(action);

        try {
            switch (action) {
                case "/search":
                    searchUser(request, response);
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertUser(request, response);
                    break;
                case "/delete":
                    deleteUser(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateUser(request, response);
                    break;
                default:
                    listUser(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }
    private void searchUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<User> allUsers = userDAO.selectAllUsers();
        String userToDisplay = request.getParameter("search");
        List<User> usersBeingSearched = new ArrayList<>();
        //get only the usersBeingSearched that is similar to the username being searched
        allUsers.stream()
                .filter(user -> user.getUsername().toLowerCase().contains(userToDisplay))
                .forEach(usersBeingSearched::add);
        if (!usersBeingSearched.isEmpty()) {
            request.setAttribute("listUser", usersBeingSearched);
            RequestDispatcher dispatcher = request.getRequestDispatcher("user-list.jsp");
            dispatcher.forward(request, response);
        } else {
            request.setAttribute("listUser", allUsers);
            RequestDispatcher dispatcher = request.getRequestDispatcher("user-list.jsp");
            dispatcher.forward(request, response);
        }
    }

    private void listUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<User> listUser = userDAO.selectAllUsers();
        request.setAttribute("listUser", listUser);
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-list.jsp");
        dispatcher.forward(request, response);

    }
    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        User existingUser = userDAO.selectUser(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("user-form.jsp");
        request.setAttribute("user", existingUser);
        dispatcher.forward(request, response);

    }

    private void insertUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        String userType = request.getParameter("inlineRadioOptions");//checks value of the selected radio button (student/teacher),
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Pattern pattern = Pattern.compile("[^A-Za-z0-9 ]");
        Matcher matcher = pattern.matcher(String.valueOf(request.getParameter("ID")));
        boolean hasSpecialChars = matcher.find();
        String userID = String.valueOf(request.getParameter("ID"));
        boolean allNumbers = checkString(userID);
        //checks for special characters and letters in id and white spaces in all input fields
        //asks for new input if any condition is true
        if (userType == null) {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
            pw.println("Please select user type (Student/Teacher).");
            pw.println("</div>");
            RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
            rd.include(request, response);
        } else if (allNumbers) {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
            pw.println("ID contains letters.");
            pw.println("</div>");
            RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
            rd.include(request, response);
        } else if (hasSpecialChars) {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
            pw.println("ID has special characters.");
            pw.println("</div>");
            RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
            rd.include(request, response);
        } else if (userID.contains(" ") || userID.isEmpty()) {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
            pw.println("ID contains white space");
            pw.println("</div>");
            RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
            rd.include(request, response);
        } else if (username.contains(" ") || username.isEmpty()) {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
            pw.println("Username contains white space");
            pw.println("</div>");
            RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
            rd.include(request, response);
        } else if (password.contains(" ") || password.isEmpty()) {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
            pw.println("Password contains white space");
            pw.println("</div>");
            RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
            rd.include(request, response);
        } else {
            if (userType.equalsIgnoreCase("student")) {
                userType = "student";
            } else {
                userType = "teacher";
            }
            allNumbers = checkString(userID);
            if (allNumbers) {
                response.setContentType("text/html");
                PrintWriter pw = response.getWriter();
                pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
                pw.println("ID has special characters.");
                pw.println("</div>");
                RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
                rd.include(request, response);
            } else {
                int id = Integer.parseInt(request.getParameter("ID"));
                //check if id is being used by a student, asks a new id if true
                if (userDAO.checkIfIDExists(id, "student") && userType.equalsIgnoreCase("student")) {
                    response.setContentType("text/html");
                    PrintWriter pw = response.getWriter();
                    pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
                    pw.println("A student already uses the ID.");
                    pw.println("</div>");
                    RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
                    rd.include(request, response);
                    //else check if id is being used by a teacher, also asks a new one if true
                } else if (userDAO.checkIfIDExists(id, "teacher") && userType.equalsIgnoreCase("teacher")) {
                    response.setContentType("text/html");
                    PrintWriter pw = response.getWriter();
                    pw.println("<div class=\"alert alert-danger\" role=\"alert\">");
                    pw.println("A teacher already uses the ID.");
                    pw.println("</div>");
                    RequestDispatcher rd = request.getRequestDispatcher("user-form.jsp");
                    rd.include(request, response);
                } else {
                    User newUser = new User(id, username, password, userType);
                    userDAO.insertUser(newUser, userType);
                    response.sendRedirect("list");
                }
            }
        }
    }


    private void updateUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("ID"));
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String userType = userDAO.getUserType(id);
        //reject username or password if it contains whitespace or is null
        if(username.isEmpty() || password.isEmpty() || username.contains(" ") || password.contains(" ")) {
            response.sendRedirect("edit?"+"id="+id);
        } else {
            User book = new User(id, username, password, userType);
            userDAO.updateUser(book);
            response.sendRedirect("list");
        }

    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String studentType = userDAO.getUserType(id);
        if (studentType.equalsIgnoreCase("student")) {
            userDAO.deleteUser(id, studentType);
        } else {
            studentType = "teacher";
            userDAO.deleteUser(id, studentType);
        }
        response.sendRedirect("list");
    }

    private boolean checkString (String id) {
        int len = id.length();
        for (int i = 0; i < len; i++) {
            // checks whether the character is not a letter
            // if it is not a letter ,it will return false
            if ((Character.isLetter(id.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }
}