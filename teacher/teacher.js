// Contains all the functions used in the teacher side
/**
 * @jest-environment jsdom
 */
const path = require('path');
const express = require('express');
const session = require('express-session');
const fileUpload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const mysql = require('mysql');
const logger = require("morgan");
const codeGenerator = require("randomstring");
const sharp = require("sharp");
const app = express();
const PORT = 12345;


const bodyParser = require('body-parser');

// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({
    extended: true
}));


const db = mysql.createPool({
    // host: 'localhost',
    host: '200okquiz.com',
    user: 'root',
    password: '',
    database: '200okquiz',
    multipleStatements: true
})

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());
app.use(session({
    secret: 'somesecretkey',
    resave: true,
    saveUninitialized: true
}));

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));

app.get('/', function(req, res) {
    let user = req.session.user;

    if (user) {
        let sql = "SELECT * FROM quizzes WHERE teacher = ?";

        db.query(sql, user.teachersID, function(err, result) {
            if (err) throw err;

            res.render('index', {
                quizzes: result,
                user: user
            });
        });
    } else {
        res.render('login');
    }
})
app.post('/login', function(req, res) {
    let username = req.body.username;
    let password = req.body.password;

    let sql = "SELECT teachersID FROM teachers WHERE username = ? AND password = ?";
    db.query(sql, [username, password], function(err, result) {
        if (err) throw err;

        if (result.length > 0) {
            req.session.user = JSON.parse(JSON.stringify(result))[0];
            res.redirect('/');
        } else {
            res.sendStatus(304);
        }
    });
});
app.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/');
});


/**
 * QUIZ CRUD
 */
app.get('/edit-quiz', function(req, res) {
    if (req.session.user) {
        let sql1 = "SELECT * FROM quizzes WHERE quizID = ?";
        let sql2 = "SELECT quizType FROM typequiz";

        db.query(sql1, [req.query.quiz], function(err, result1) {
            db.query(sql2, function(err, result2) {
                req.session.quiz = JSON.parse(JSON.stringify(result1))[0];
                res.render('edit-quiz', {
                    quiz: req.session.quiz,
                    types: result2
                });
            });
        });
    } else {
        res.redirect('/');
    }

});
app.post('/add-quiz', function(req, res) {
    if (req.session.user) {
        let sql1 = "SELECT * FROM quizzes WHERE quizCode = ?";
        let sql2 = "SELECT COUNT(*) count FROM quizzes";
        let sql3 = "INSERT INTO quizzes (quizID, quizName, status, quizCode, teacher) VALUES (?,?,?,?,?)";
        let code, numRows;

        do {
            code = codeGenerator.generate(9);

            db.query(sql1, code, function(err, result) {
                if (err) throw err;
                numRows = result.length;
            });
        } while (numRows > 0)


        db.query(sql2, function(err, result) {
            if (err) throw err;

            let count = JSON.parse(JSON.stringify(result))[0].count + 1;

            db.query(sql3, [count, req.body.topic, 'Active', code, req.session.user.teachersID], function(err) {
                if (err) throw err;
            });
        });

        res.redirect('/');
    } else {
        res.redirect('/');
    }

});
app.post('/update-quiz', function(req, res) {
    if (req.session.user) {

        let sql = "UPDATE quizzes SET quizName = ?, dateOpen = ?, dateClose = ? WHERE quizID = ?";
        db.query(sql, [req.body.quizTitle, req.body.quizOpen, req.body.quizClose, req.session.quiz.quizID], function(err) {
            if (err) {
                res.sendStatus(304);
            }
        });
        res.redirect('back');
    } else {
        res.redirect('/');
    }

});
app.post('/delete-quiz', function(req, res) {
    if (req.session.user) {
        let sql1 = "DELETE FROM quizzes WHERE quizID = ?";
        let sql2 = "SET @quizID = 0";
        let sql3 = "SET @questionID = 0";
        let sql4 = "SET @choiceID = 0";
        let sql5 = "UPDATE quizzes SET quizID = @quizID := (@quizID+1)";
        let sql6 = "UPDATE question SET questionID = @questionID := (@questionID+1)";
        let sql7 = "UPDATE qdetails SET choiceID = @choiceID := (@choiceID+1)";

        db.query(sql1, req.body.quiz);
        db.query(sql2);
        db.query(sql3);
        db.query(sql4);
        db.query(sql5);
        db.query(sql6);
        db.query(sql7);

        res.redirect('/');
    } else {
        res.redirect('/');
    }

});
app.post('/set-quiz-status', function(req, res) {
    if (req.session.user) {
        let sql = "UPDATE quizzes SET status = ? WHERE quizID = ?";
        db.query(sql, [req.body.status, req.body.quiz]);

        res.redirect('/');
    } else {
        res.redirect('/');
    }
});


/**
 * GETTING DIFFERENT CATEGORY PAGES
 */
app.get('/edit-multiple-choice', function(req, res) {
    getEditorPage("a", req.session.quiz, "edit-multiple-choice", res, req);
})
app.get('/edit-tof', function(req, res) {
    getEditorPage("b", req.session.quiz, "edit-tof", res, req);

})
app.get('/edit-identification', function(req, res) {
    getEditorPage("c", req.session.quiz, "edit-identification", res, req);
})
app.get('/edit-fib', function(req, res) {
    getEditorPage("d", req.session.quiz, "edit-fib", res, req);
})
app.get('/edit-matching-type', function(req, res) {
    getEditorPage("e", req.session.quiz, "edit-matching-type", res, req);
})


/**
 * QUESTION CRUD
 */
app.post('/add-question', async function(req, res) {
    if (req.session.user) {
        const question = req.body;
        let image = null;

        if (req.files) {
            const {
                data,
                info
            } = await sharp(req.files.image.data)
                .resize(200, 200, {
                    fit: 'outside'
                })
                .jpeg()
                .toBuffer({
                    resolveWithObject: true
                });

            image = data;
        }

        insertQuestion(question, image, question.type, req);

        res.redirect('back');
    } else {
        res.redirect('/');
    }

});

app.post('/update-questions', function(req, res) {
    if (req.session.user) {
        let questions = req.body;

        if (!questions.title) {
            res.sendStatus(304);
        } else {
            questions.quiz = req.session.quiz.quizID;
            let files = req.files;

            if (req.files !== null) {
                files = req.files;
            }

            update(questions, questions.type, files);

            res.redirect('back');
        }
    } else {
        res.redirect('/');
    }
});
app.post('/delete', function(req, res) {
    if (req.session.user) {
        let sql1 = "DELETE FROM question WHERE questionID = ?";
        let sql2 = "SET @questionID = 0; UPDATE question SET questionID = @questionID := @questionID+1";
        let sql3 = "SET @choiceID = 0; UPDATE qdetails SET choiceID = @choiceID :=@choiceID+1";

        db.query(sql1, req.body.id);
        db.query(sql2, function (err) {
            if (err) {
                throw err;
            }
        });
        db.query(sql3, function (err) {
            if (err) {
                throw err;
            }
        });
        res.redirect('back');
    } else {
        res.redirect('/');
    }
});


/**
 * STUDENTS
 */
app.get('/students', function(req, res) {
    if (req.session.user) {
        let sql1 = "SELECT studentID, username FROM students";
        let sql2 = "SELECT * FROM quizzes";
        db.query(sql1, (err, result1) => {
            db.query(sql2, (err, result2) => {
                if (err) throw err;
                res.render('students.ejs', {
                    students: result1,
                    quizzes: result2
                });
            });
        });

    } else {
        res.redirect('/');
    }
})



app.get('/view', async function(req, res) {

    if (req.session.user) {


        let query = "DROP TABLE IF EXISTS statistics;";
        let create = "CREATE TABLE statistics SELECT idNum, username as Name, queID as QueID, answers, statusM as `Status`, quizType, quizID, quizName FROM studentquizlogs JOIN students JOIN question JOIN quizzes JOIN typequiz WHERE students.studentID = studentquizlogs.idNum AND question.questionID = studentquizlogs.queID AND quizzes.quizID = question.quiz  AND typequiz.quizTypeID = studentquizlogs.quizTID ORDER BY idNum, queID;";
        let selectQuery = "SELECT idNum, Name, QueID, answers, Status, quizType, quizName FROM statistics WHERE idNum = ? AND quizType = ? AND quizName= ?;";
        let selectScore = "SELECT score FROM studentattempt JOIN typequiz WHERE studentattempt.quizTypeID = typequiz.quizTypeID AND typequiz.quizType = ? AND studentID = ?;";

        let studentIDNum = req.query.idNum;
        let qtype = req.query.quizType;
        let qName = req.query.qzName;
        db.query(query, (err, result1) => {
            db.query(create, (err, result2) => {
                db.query(selectQuery, [studentIDNum, qtype, qName], function(err, data) {
                    db.query(selectScore, [qtype, studentIDNum], function(err, data1) {
                        if (err) throw err;
                        res.render('view-answers.ejs', {
                            title: 'View Answer',
                            action: 'list',
                            type: qtype,
                            sampleData: data,
                            score: data1
                        });
                    });
                });
            });
        });


    } else {
        res.redirect('/');
    }
})


/**
 * STATISTICS
 */
app.get("/statistics", async function(req, res, next) {
    if (req.session.user) {
        let query = "DROP TABLE IF EXISTS statistics;";
        let create = "CREATE TABLE statistics SELECT idNum, username as Name, queID as QueID, answers, statusM as `Status`, quizType, quizID, quizName FROM studentquizlogs JOIN students JOIN question JOIN quizzes JOIN typequiz WHERE students.studentID = studentquizlogs.idNum AND question.questionID = studentquizlogs.queID AND quizzes.quizID = question.quiz  AND typequiz.quizTypeID = studentquizlogs.quizTID ORDER BY idNum, queID;";
        let selectQuery = "SELECT * FROM statistics;";

        db.query(query, (err, result1) => {
            db.query(create, (err, result2) => {
                db.query(selectQuery, (err, data) => {
                    if (err) throw err;
                    res.render('statistics', {
                        title: 'Student Logs',
                        action: 'list',
                        sampleData: data
                    });
                });
            });
        });


    } else {
        res.redirect('/');
    }
});
app.get("/countCorrect", function(req, res, next) {
    if (req.session.user) {
        let selectQuery = " SELECT QueID, questions as Question, COUNT(`Status` = 'correct') as `correct` FROM question INNER JOIN statistics WHERE  question.questionID = statistics.QueID AND `Status` = 'correct' GROUP BY QueID;";
        db.query(selectQuery, function(error, data) {

            if (error) {
                throw error;
            } else {
                res.render('count-correct', {
                    title: 'Correct Count',
                    action: 'list',
                    sampleData: data
                });
            }

        });

    } else {
        res.redirect('/');
    }
});
app.get("/countIncorrect", function(req, res, next) {
    if (req.session.user) {
        let selectQuery = " SELECT QueID, questions as Question, COUNT(`Status` = 'incorrect') as `incorrect` FROM question INNER JOIN statistics WHERE  question.questionID = statistics.QueID AND `Status` = 'incorrect' GROUP BY QueID;";
        db.query(selectQuery, function(error, data) {

            if (error) {
                throw error;
            } else {
                res.render('count-incorrect', {
                    title: 'Incorrect Count',
                    action: 'list',
                    sampleData: data
                });
            }

        });

    } else {
        res.redirect('/');
    }
});


/**
 * FUNCTIONS
 */


function goToViewAnswers() {
    window.location.replace('http://localhost/view');
}

function getEditorPage(type, quiz, file, res, req) {
    if (req.session.user) {
        let sql1 = "SELECT questionID, questions, CONCAT('data:image;base64,', TO_BASE64(image)) as image, points, qTypeID, quiz FROM question WHERE qTypeID = ? AND quiz = ?;";
        let sql2 = "SELECT * FROM qdetails JOIN question ON question = questionID WHERE qTypeID = ? AND quiz = ?;"

        db.query(sql1, [type, quiz.quizID], function(err, result1) {
            if (err) throw err;

            db.query(sql2, [type, quiz.quizID], function(err, result2) {
                if (err) throw err;

                res.render(file, {
                    questions: result1,
                    qChoices: result2,
                });
            });
        });
    } else {
        res.redirect('/');
    }
}

function insertQuestion(question, image, type, req) {
    let sql1 = "INSERT INTO question VALUES (?, ?, ?, ?, ?, ?, ?)";
    let sql2 = "SELECT COUNT(*) count FROM question";
    let sql3 = "SELECT points FROM question WHERE qTypeID = ? LIMIT 1";

    db.query(sql2, function(err, result1) {
        if (err) throw err;
        db.query(sql3, type, function(err, result2) {
            if (err) throw err;

            let count = JSON.parse(JSON.stringify(result1))[0].count + 1;
            let points = JSON.parse(JSON.stringify(result2))[0].points;

            db.query(sql1, [count, question.title, image, points, type, req.session.quiz.quizID, req.session.quiz.quizCode], function(err) {
                if (err) throw err;
            });

            insertChoices(question, count, type);
        });
    });
}

function insertChoices(question, questionID, type) {
    let sql4 = "INSERT INTO qdetails VALUES (?, ?, ?, ?)";
    let sql5 = "SELECT COUNT(*) count FROM qDetails";

    let answer = question.correctAnswer;

    db.query(sql5, function(err, choices) {
        if (err) throw err;

        let count = JSON.parse(JSON.stringify(choices))[0].count + 1;

        if (type === 'a' || type === 'e') {
            question.choice.forEach(function(choice, index) {
                let choiceIndex = index + 1;
                let status = answer === choiceIndex.toString() ? "correct" : "incorrect";

                db.query(sql4, [count, questionID, choice, status], function(err) {
                    if (err) throw err;
                });
                count = count + 1;
            });
        } else if (type === 'b') {
            let status = answer === "True" ? "correct" : "incorrect";

            db.query(sql4, [count, questionID, "True", status]);

            count = count + 1;

            if (status === "correct") {
                db.query(sql4, [count, questionID, "False", "incorrect"]);
            } else {
                db.query(sql4, [count, questionID, "False", "correct"]);
            }
        } else if (type === 'c' || type === 'd') {
            db.query(sql4, [count, questionID, answer, ""]);
        }

    });

}

function update(questions, type, files) {
    updatePoints(questions, type);
    updateTitles(questions);
    updateImages(questions, files);
    updateCorrectAnswer(questions);

    if (type === 'a' || type === 'e') {
        updateChoices(questions);
    }
}

function updatePoints(questions, type) {
    let sql = "UPDATE question SET points = ? WHERE qTypeID = ? AND quiz = ?";
    db.query(sql, [questions.points, type, questions.quiz], function(err) {
        if (err) throw err;
    });
}

function updateTitles(questions) {
    let sql = "UPDATE question SET questions = ? WHERE questionID = ?";

    if (!(questions.title instanceof Array)) {
        db.query(sql, [questions.title, questions.questionIDs]);
    } else {
        for (let i = 0; i < questions.title.length; i++) {

            let id = questions.questionIDs[i];
            db.query(sql, [questions.title[i], id]);
        }
    }
}

async function updateImages(questions, files) {
    let sql = "UPDATE question SET image = ? WHERE questionID = ?";

    let images = null;

    if (files) {
        if (!files.image.length) {
            const {
                data,
                info
            } = await sharp(files.image.data)
                .resize(200, 200, {
                    fit: 'outside'
                })
                .jpeg()
                .toBuffer({
                    resolveWithObject: true
                });

            images = data;
        } else {
            images = [];

            for (let i = 0; i < files.image.length; i++) {
                const {
                    data,
                    info
                } = await sharp(files.image[i].data)
                    .resize(200, 200, {
                        fit: 'outside'
                    })
                    .jpeg()
                    .toBuffer({
                        resolveWithObject: true
                    });

                images.push(data);
            }
        }

    }

    let changedImages = 0;
    let changed = [];
    let removed = [];
    let imageChange = 0;

    if (!(questions.title instanceof Array)) {
        let id = questions.questionIDs;

        if (eval("questions.imageState" + id) === "changed") {
            changedImages++;
            changed.push(id);
        } else if (eval("questions.imageState" + id) === "removed") {
            removed.push(id);
        }
    } else {
        for (let i = 0; i < questions.title.length; i++) {
            let id = questions.questionIDs[i];

            if (eval("questions.imageState" + id) === "changed") {
                changedImages++;
                changed.push(id);
            } else if (eval("questions.imageState" + id) === "removed") {
                removed.push(id);
            }
        }
    }

    for (const index of changed) {

        if (changedImages > 1) {

            await db.query(sql, [images[imageChange], index]);
            imageChange++;
        } else {
            await db.query(sql, [images, index]);
        }

    }

    for (const index of removed) {
        await db.query(sql, [null, index]);
    }
}

function updateCorrectAnswer(questions) {
    let sql = "UPDATE qDetails SET status = CASE WHEN choiceID = ? THEN 'correct' " + "WHEN choiceID != ? THEN 'incorrect' END WHERE question = ?";

    if (!(questions.title instanceof Array)) {
        let id = questions.questionIDs;
        db.query(sql, [eval("questions.choice_q" + id), eval("questions.choice_q" + id), id]);
    } else {
        for (let i = 0; i < questions.title.length; i++) {
            let id = questions.questionIDs[i];
            db.query(sql, [eval("questions.choice_q" + id), eval("questions.choice_q" + id), id]);
        }
    }
}

function updateChoices(questions) {
    let sql = "UPDATE qDetails SET choice = ? WHERE choiceID = ?";
    for (let i = 0; i < questions.choice.length; i++) {
        let id = questions.choiceIDs[i];
        db.query(sql, [questions.choice[i], id]);
    }
}