(() => {
    'use strict'
    feather.replace({'aria-hidden': 'true'});
})();

document.addEventListener('DOMContentLoaded', () => {

    let myBtns = document.querySelectorAll('.nav-link');
    myBtns.forEach(function (btn) {

        btn.addEventListener('click', () => {
            myBtns.forEach(b => b.classList.remove('active'));
            btn.classList.add('active');
        });

    });
});

const linkedPicker1Element = document.getElementById('linkedPickers1');
const linked1 = new tempusDominus.TempusDominus(linkedPicker1Element, {
    restrictions: {
        minDate: new Date()
    }
});
const linked2 = new tempusDominus.TempusDominus(document.getElementById('linkedPickers2'), {
    useCurrent: false,
});

linkedPicker1Element.addEventListener(tempusDominus.Namespace.events.change, (e) => {
    linked2.updateOptions({
        restrictions: {
            minDate: e.detail.date,
        }
    });
});

linked2.subscribe(tempusDominus.Namespace.events.change, (e) => {
    linked1.updateOptions({
        restrictions: {
            maxDate: e.date,
        }
    });
});



function discard() {
    location.reload();
}

function save() {
    alert("Successfully saved changes.");
}

function setNumberOfChoices() {
    let choices = document.getElementById("choices");
    let n = document.getElementById("number");

    for (let i = 0; i < n.value; i++) {
        choices.innerHTML += "<div class='input-group mb-3'> <div class='input-group-text'> <input class='form-check-input' type='radio' name='correctAnswer' aria-label='Radio button for following text input' value='<%= i %>' required> </div><input type='text' class='form-control' placeholder='Choice' name='choice' aria-label='Choice' aria-describedby='basic-addon1 ' required></div>"
    }
}

function uploadImage(questionID) {
    let image = document.querySelector("#img" + questionID);
    let fileInput = document.querySelector("#inputFile" + questionID);
    let imageState = document.querySelector("#imageState" + questionID);

    image.src = URL.createObjectURL(fileInput.files[0]);
    imageState.value = "changed";
}

function removeImage(questionID) {
    let image = document.querySelector("#img" + questionID);
    let imageState = document.querySelector("#imageState" + questionID);

    image.src = "";
    imageState.value = "removed";
}

function deleteQuestion(type, questionID) {

    if (confirm("Are you sure you want to delete? You cannot undo this action.")) {
        fetch('/delete', {
            method: 'POST', headers: {
                Accept: 'application/json', 'Content-Type': 'application/json'
            }, body: JSON.stringify({
                "id": questionID, "type": type
            })
        }).then(res => {

            location.href = res.url;
        });
    }
}

function updateQuiz() {
    let form = document.getElementById("updateQuizForm");
    const data = new FormData(form);
    const moment1 = new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false,

    }).formatToParts(linked1.viewDate).reduce((acc, part) => {
        acc[part.type] = part.value;
        return acc;
    }, {});
    const moment2 = new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,

    }).formatToParts(linked2.viewDate).reduce((acc, part) => {
        acc[part.type] = part.value;
        return acc;
    }, {});
    data.append('quizOpen', `${moment1.year}-${moment1.month}-${moment1.day} ${moment1.hour}:${moment1.minute}:00`);
    data.append('quizClose', `${moment2.year}-${moment2.month}-${moment2.day} ${moment2.hour}:${moment2.minute}:00`);

    fetch('/update-quiz', {
        method: 'POST',
        body: data
    }).then(res => {
        if (!res.ok) {
            alert("Please enter a valid date and time by clicking on the calendar icon.");
        } else {
            location.href = res.url;
            save();
        }
    });
}

function updateQuestions() {
    let form = document.getElementById("updateForm");
    const data = new FormData(form);

    fetch('/update-questions', {
        method: 'POST',
        redirect: 'follow',
        body: data
    }).then(res => {

        console.log(res.status);

        if (res.status === 304) {
            alert("Please add a question.");
        } else {
            location.href = res.url;
        }
        /*
        if (res.status === 400) {
            alert("Please add a question.");
        }
        location.href = res.url;
        save();*/
    });
}
function addQuestion() {
    let form = document.getElementById("addForm");
    const data = new FormData(form);

    fetch('/add-question', {
        method: 'POST',
        body: data
    }).then(res => {
        location.href = res.url;
    });
}

function reload() {
    const reload = setTimeout(location.reload(), 1000 );
}

