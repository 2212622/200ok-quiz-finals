/*
Referenced code for the homepage: https://github.com/sefyudem/Sliding-Sign-In-Sign-Up-Form
Modified by: Mendoza, Ivanka Chaental Louise
*/

const container = document.querySelector(".container");
const showPasswordTeacher = document.querySelector("#show-password-teacher");
const passwordFieldTeacher = document.querySelector("#password-teacher");
showPasswordTeacher.addEventListener("click", function(){
    this.classList.toggle("fa-eye-slash");
    const type = passwordFieldTeacher.getAttribute("type")
    === "password" ? "text" : "password";
    passwordFieldTeacher.setAttribute("type", type);
});

function login() {
    let username = document.getElementById("username-teacher");
    let password = document.getElementById("password-teacher");

    fetch('/login', {
        method: 'POST', redirect: 'follow', headers: {
            Accept: 'application/json', 'Content-Type': 'application/json'
        }, body: JSON.stringify({
            "username": username.value,
            "password": password.value
        })
    }).then(res => {
        if (!res.ok) {
            alert("The username/password is incorrect.");
        } else {
            location.href = res.url;
        }
    });
}

function goToStudentPage() {
    window.location.replace('http://200okquiz.com');
}