<?php
/**
 * get_quiz.php
 * Checks if the quiz is open or closed and if the entered code is valid.
 * Redirects a student to the quiz proper.
 */
if(!isset($_SESSION))
{
    session_start();
}
include "includes/db.php";
include "Quiz.php";
$db = new Quiz();
$quizID = $_SESSION['quizID'];
if(isset($_GET['submit'])){
    $quizCode = $_GET['code'];
    $result = $db->get_quizzes($quizCode, $quizID);
    if($result-> num_rows > 0) {
        while ($row = $result-> fetch_assoc()) {
            if($row['quizCode'] == $quizCode && $row['quizID'] == $quizID)
            {
                date_default_timezone_set("Asia/Manila");
                $current =  date('Y-m-d H:i:s');
                $curr = $current;
                $open = $row['dateOpen'];
                $close = $row['dateClose'];

                if($curr < $open || is_null($open)){
                    echo ("<script>window.alert('Quiz has not started yet')
                                    window.location.href='home.php?q=1'</script>");
                }
                else if($curr > $close){
                    echo ("<script>window.alert('Quiz has expired')
                        window.location.href='home.php?q=1'</script>");
                }
                else
                {
                        $sess_code = $_SESSION['code'];
                        header("Location: student_quiz.php?quizCode=$quizCode");

                }

            }
        }
    }
    else{
            echo ("<script>window.alert('Invalid Code!')
                        window.location.href='code_form.php?qid=$quizID'</script>");
    }
    $db->close();
}
?>
