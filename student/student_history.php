<?php
/**
 * Student History Page
 * Displays and redirects to the results of the type of quiz
 * associated with the main topic.
 */
if(!isset($_SESSION))
{
    session_start();
}
$studentID = $_SESSION["studentID"];
$quizID = $_GET['qid'];

include "includes/db.php";
include "Quiz.php";
$db = new Quiz();
$result = $db->get_quiz($quizID);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style/sidebar.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <script src="check-session.js"></script>
</head>
<body>

<div class="sidebar">
    <header>Not Answered</header>
    <?php
    while($row = $result->fetch_assoc()) {
        $code = $row['quizCode'];
        $_SESSION['quizCode'] = $code;
    }
    $result = $db->get_available_quiz($quizID, $studentID, $code);
    while($row = mysqli_fetch_array($result)) {
        echo '<a href=results/results.php?type='.$row['quizTypeID'].'><i class="fas fa-pencil-alt"></i><span>' .$row['quizType']. '</span></a>';
    }
    ?>
    <header>Answered</header>
    <?php
    $result = $db->get_done_quiz($quizID, $studentID, $code);
    while($row = mysqli_fetch_array($result)) {
        echo '<a href=results/results.php?type='.$row['quizTypeID'].'><i class="fas fa-check"></i><span>' .$row['quizType']. '</span></a>';
    }
    ?>
    <input type="text" value="Return Home" name="submit" onclick="location.href='home.php?q=1'" class="btn"/>
</div>
</body>
</html>

