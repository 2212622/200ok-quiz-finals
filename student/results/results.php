<!--
 * View Results Page
 * Displays the quiz result.
 * Serves as a quiz review.
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <link rel="stylesheet" href="../style/body.css">
    <link rel="stylesheet" href="../style/questions.css">
    <script src="../check-session.js"></script>
</head>
<body>
<div class="quiz-container">
    <div class="inner-width">

        <table class="align-middle mb-0 table table-borderless table-striped table-hover" id="tableList">
            <?php
            if(!isset($_SESSION))
            {
                session_start();
            }
            if(isset($_SESSION['status']))
            {
                echo "<h4>".$_SESSION['status']. "</h4>";
                unset($_SESSION['status']);
            }
            $studentID = $_SESSION['studentID'];
            $quizCode=$_SESSION['quizCode'];
            include("../includes/db.php");
            include '../Quiz.php';
            $db = new Quiz();
            $quizType=$_GET['type'];

            $questions = $db->get_questions($quizCode, $quizType);
            $user_answers = $db->get_user_answers($quizType, $studentID, $quizCode);
            $score =$db->get_score($quizType,$studentID, $quizCode);
            $user_score = mysqli_fetch_array($score);

            $totalScore = 0;
            if ($user_answers->num_rows ==0) {
                echo "<script>alert('Please answer the quiz first');
                        window.history.back()</script>";
            } else {
                if ($quizType == 'a' || $quizType == 'b' || $quizType == 'e') {
                $correct_answers = $db->get_answers('correct', $quizType, $quizCode);
                    $i=1;
                    while ($row=$questions->fetch_assoc()) { ?>
                        <tr>
                            <td>
                                <b><p><?php echo $i; ?> .)  <?php echo $row['questions']; ?></p></b>
                                <label class="pl-4 text-success">
                                    <span style="color:blue">Answer: </span>
                                    <?php
                                    $cor_ans = mysqli_fetch_array($correct_answers);
                                    $user_ans = mysqli_fetch_array($user_answers);
                                    $totalScore = $totalScore + $row['points'];
                                    if ($user_ans['answers'] != $cor_ans['answers']) {
                                        ?>
                                        <span style="color:red"><?php echo $user_ans['answers']; ?>
                                    <span style="color:#28a745" "><?php
                                            echo nl2br("\n Correct Answer: ". $cor_ans['answers']); ?> </span>
                                        </span>
                                    <?php
                                    } else {
                                        ?>
                                        <span class="text-success"><?php echo $user_ans['answers']; ?></span>
                                    <?php }
                                    ?>
                                </label>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                } else if ($quizType == 'c' || $quizType == 'd') {
                $correct_answers = $db->get_answers_others($quizType, $quizCode);
                    $i=1;
                    while ($row = $questions->fetch_assoc()) { ?>
                        <tr>
                            <td>
                                <b><p><?php echo $i; ?> .)  <?php echo $row['questions']; ?></p></b>
                                <label class="pl-4 text-success">
                                    <span style="color:blue">Answer: </span>
                                    <?php
                                    $cor_ans=mysqli_fetch_array($correct_answers);
                                    $user_ans=mysqli_fetch_array($user_answers);

                                    $lower_case_cor_ans = strtolower($user_ans['answers']);
                                    $lower_case_user_ans =strtolower($cor_ans['answers']);
                                    $totalScore = $totalScore + $row['points'];
                                    if (strcmp($lower_case_user_ans, $lower_case_cor_ans) !== 0) {?>
                                        <span style="color:red"><?php echo $user_ans['answers']; ?>
                                        <span style="color:#28a745" "><?php
                                            echo nl2br("\n Correct Answer: ". $cor_ans['answers']); ?> </span>
                                        </span>
                                    <?php
                                    } else {?>
                                        <span class="text-success"><?php echo $user_ans['answers']; ?></span>
                                    <?php
                                    }
                                    ?>
                                </label>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
            }
            ?>
            <h2><span class="score">Score: <?php echo $user_score['score']."/".$totalScore ?></span></h2>
        </table>
        <div class="button">
            <input type="submit" value="Back" name="submit" onclick="window.history.back()" class="btn"/>
        </div>
    </div>
</div>
</body>
</html>

