<?php
/**
 * Logout
 * Destroys a student session and
 * redirects to the student login page.
 */
session_id($_SESSION['session_id']);
session_start();
session_destroy();
header("location:../index.php");
?>
<script>alert ('Duplicate login! You have been logged out.');</script>
