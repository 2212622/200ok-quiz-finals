<?php
/**
 * get_cards.php
 * Displays the quiz cards.
 * Author/s:
 */
    if (!isset($_SESSION)) {
        session_start();
    }
    $studentID = $_SESSION['studentID'];
    include "includes/db.php";
    $quizzes = $db->query("SELECT * FROM quizzes");
    $result = $db->query("SELECT DISTINCT quizName, q.quizID FROM quizzes q JOIN studentattempt sa ON q.quizID = sa.quizID where studentID = '$studentID'");
    if (@$_GET["q"] == 1) {
        echo "<h1>Available Quizzes</h1>";
        echo '<ul class="cards" id="cards">';
            while ($row = $quizzes->fetch_assoc()) {
                if ($row["status"] == "Active") {
                    echo '<li class="cards_item">';
                    echo '<div class="card"><div class="card_image">';
                    echo '<img src="style/images/card.png" alt="alt message" class="responsive"></div>';
                    echo '<div class="card_content"><h2 class="card_title">'.$row["quizName"].'</h2>';

                    $_SESSION["quizID"] = $row["quizID"];
                    echo '<a href="code_form.php?qid='.$row["quizID"].'" class="btn card_btn">Join Quiz</a></div></div></li>';
                }
            }
        echo "</ul>";
    } elseif (@$_GET["q"] == 2) {
        echo "<h1>Taken Quizzes</h1>";
        echo '<ul class="cards" id="cards">';
            while ($quiz = $result->fetch_assoc()) {
                    echo '<li class="cards_item">';
                    echo '<div class="card"><div class="card_image">';
                    echo '<img src="style/images/card1.png" alt="alt message" class="responsive"></div>';
                    echo '<div class="card_content"><h2 class="card_title">' . $quiz["quizName"] . '</h2>';

                    $_SESSION["quizID"] = $quiz["quizID"];
                    echo '<a href="student_history.php?qid=' . $quiz["quizID"] . '" class="btn card_btn">View Results</a></div></div></li>';
            }
        echo "</ul>";
    }
?>