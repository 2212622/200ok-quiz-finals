<?php
/**
 * History Page
 * Displays a student's quiz log/past quizzes.
 */
    session_start();

    if (!isset($_SESSION["session_id"])) {
        header("location:index.php");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style/body.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Patrick Hand SC">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="check-session.js"></script>

    <script>
        setInterval(function() {
            $("#main").load('get_cards.php?q=2');
        }, 1000);
    </script>
</head>
<body>

<?php
include "includes/sidebar.php"; // sidebar
include "includes/header.php"; // header
?>
<div class="main" id="main">
<!-- contents found in get-cards.php -->
</div>
</body>
</html>

