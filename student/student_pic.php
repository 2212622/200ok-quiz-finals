<?php
/**
 * Upload Picture Page
 * Allows a student to modify his/her display picture.
 */
if(!isset($_SESSION))
{
    session_start();
}
$studentID = $_SESSION["studentID"];
$username = $_SESSION["username"];
include "includes/db.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture</title>
    <link rel="stylesheet" href="style/upload.css">
    <script src="check-session.js"></script>
</head>
<body>
<div class="container">
    <?php
    if(isset($_POST['upload'])) {
        $status = 'ok';
        if ($_FILES['file']['error']==4){
            echo "<script>alert('Please upload a file!');
                        window.history.back()</script>";
            $status = 'error';
        }
        $fname = $_FILES["file"]["name"];
        $ftype = pathinfo($fname,PATHINFO_EXTENSION);
        $fsize = $_FILES["file"]["size"];

        if($ftype != "jpg" && $ftype != "png" && $ftype != "jpeg"
            && $ftype != "gif" ) {
            echo "<script>alert('Sorry, only GIF, JPG, and PNG files are allowed.');
                        window.history.back()</script>";
            $status = 'error';
        }
        if($fsize > 16384) {
            echo "<script>alert('Your file is larger than 16 MB.');
                        window.history.back()</script>";
            $status = 'error';
        }
        if ($status == 'ok') {
            $temp = explode(".", $_FILES["file"]["name"]);
            $tmp = $_FILES["file"]["tmp_name"];
            $newfname = $studentID. '.' . end($temp);
            $dest = __DIR__ . "/style/images/$newfname";
            $image = addslashes(file_get_contents($tmp));
            $move = move_uploaded_file($tmp, $dest);

            if ($move) {
                $sql = "UPDATE students SET image = '$image' WHERE studentID = '$studentID'";

                $qry = mysqli_query($db, $sql);
                echo "<script>alert('Your profile picture has been updated.');
                        window.history.back()</script>";
            }
        }
    }
    ?>

    <div class="card">
        <h3>Upload Profile Picture</h3>
        <div class="drop_box">
            <header>
                <h4>Select File here</h4>
            </header>
            <p>Files Supported: GIF, JPG, PNG</p>
            <form action="" method="post" enctype="multipart/form-data" >
                <input type="file" name="file" accept="image/png,image/jpeg,image/gif" id="fileID"/>

                <input type="submit" name="upload" value="upload" />
            </form>
            <br>
            <a onclick="window.history.back()" class="button btn3">Back</a>
        </div>
    </div>
</div>
</body>
</html>

