<?php
/**
 * Home Page
 * Displays the Header, Sidebar (Menu), and Active quizzes.
 */
    session_start();

    if (!isset($_SESSION["session_id"])) {
        header("location:index.php");
    }

    $username = $_SESSION["username"];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Hello, <?php echo $username; ?>!</title>
    <link rel="stylesheet" href="style/body.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Patrick Hand SC">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="check-session.js"></script>
    <script>
        setInterval(function() {
            $("#main").load('get_cards.php?q=1'); // Displays active quizzes
        }, 1000);
    </script>
</head>
<body>
<?php
include "includes/sidebar.php"; // sidebar
include "includes/header.php"; // header
?>
<div class="main" id="main">
    <!-- contents found in get_cards.php -->
</div>
</body>
</html>