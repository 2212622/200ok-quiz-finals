<?php
if(!isset($_SESSION))
{
    session_start();
}
class Quiz
{


// Database credentials
    private $host = 'localhost';
    private $username = 'root';
    private $password = '';
    private $database = '200okquiz';
    public $db;

    public function __construct()
    {
        if (!isset($this->db)) {
            // Connect to the database
            try {
                $this->db = new mysqli($this->host, $this->username, $this->password, $this->database);
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }

        }
    }

    public function get_questions($quizCode, $quizType)
    {
        $select = $this->db->prepare("SELECT * FROM `question` where quizCode=? AND qTypeID=?");
        $select->bind_param('ss', $quizCode, $quizType);
        $select->execute();
        return $select->get_result();
    }

    public function get_answers($statusMark, $quizType, $quizCode){
        $select = $this->db->prepare("SELECT question, choice as answers, statusMark FROM qdetails JOIN question ON qdetails.question = question.questionID WHERE statusMark = ? AND qTypeID = ? AND quizCode = ?");
        $select->bind_param('sss', $statusMark, $quizType, $quizCode);
        $select->execute();
        return $select->get_result();
    }

    public function get_answers_others($quizType, $quizCode){
        $select = $this->db->prepare("SELECT question, choice as answers FROM qdetails JOIN question ON qdetails.question = question.questionID WHERE qTypeID = ? AND quizCode=?");
        $select->bind_param('ss', $quizType, $quizCode);
        $select->execute();
        return $select->get_result();
    }

    public function get_user_answers($quizType, $studentID, $quizCode){
        $select = $this->db->prepare("SELECT answers FROM studentquizlogs sq JOIN question q ON sq.queID = q.questionID WHERE quizTID = ? AND idNum = ? AND quizCode=?");
        $select->bind_param('sss', $quizType, $studentID, $quizCode);
        $select->execute();
        return $select->get_result();
    }

    public function get_score($quizType,$studentID, $quizCode){
        $select = $this->db->prepare("SELECT score FROM studentattempt WHERE quizTypeID = ? AND studentID = ? AND quizCode=?");
        $select->bind_param('sss', $quizType,$studentID, $quizCode);
        $select->execute();
        return $select->get_result();
    }

    public function get_quiz($quizID){
        $select = $this->db->prepare("SELECT * from quizzes WHERE quizID = ?");
        $select->bind_param('i', $quizID);
        $select->execute();
        return $select->get_result();
    }

    public function get_quizzes($quizCode, $quizID){
        $select = $this->db->prepare("SELECT * FROM quizzes WHERE quizCode = ? AND quizID = ?");
        $select->bind_param('ss', $quizCode, $quizID);
        $select->execute();
        return $select->get_result();
    }

    public function get_available_quiz($quizID, $studentID, $quizCode){
        $select = $this->db->prepare("SELECT DISTINCT quizTypeID,quizType FROM typeQuiz tq  
                                            JOIN question q ON tq.quizTypeID=q.qTypeID WHERE quiz = ? 
                                            AND tq.quizTypeID NOT IN(SELECT quizTypeID from studentattempt 
                                            WHERE studentID = ? AND quizCode = ?)");
        $select->bind_param('iss', $quizID, $studentID, $quizCode);
        $select->execute();
        return $select->get_result();
    }

    public function get_done_quiz($quizID, $studentID, $quizCode){
        $select = $this->db->prepare("SELECT DISTINCT quizTypeID,quizType FROM typeQuiz tq 
                                            JOIN question q ON tq.quizTypeID=q.qTypeID WHERE quiz = ?
                                            AND tq.quizTypeID  IN(SELECT quizTypeID from studentattempt 
                                            WHERE studentID = ? AND quizCode = ?)");
        $select->bind_param('iss', $quizID, $studentID, $quizCode);
        $select->execute();
        return $select->get_result();
    }

    public function check_Attempt($studentID, $quizType, $quizID)
    {
        $select = "SELECT * FROM studentattempt where studentID = '$studentID' and quizTypeID ='$quizType' and quizID='$quizID'";
        $result = mysqli_query($this->db, $select);
        if (mysqli_fetch_all($result)) {
            return true;
        };
    }

    public function choiceID($qid)
    {
        $select = $this->db->prepare("SELECT * FROM `qdetails` , `question` where questionID = question AND question = ?");
        $select->bind_param('s', $qid);
        $select->execute();
        return $select->get_result();
    }

    public function get_pic($studentID)
    {
        $select = "SELECT * FROM students where studentID ='$studentID'";
        return mysqli_query($this->db, $select);
    }

}
?>