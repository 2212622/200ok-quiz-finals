<?php
/**
 * Student Log
 * Records the student attempt and answers into the database.
 */
if(!isset($_SESSION))
{
    session_start();

}
$studentID = $_SESSION["studentID"];
$quizType = $_SESSION['quizType'];
$quizCode = $_SESSION['quizCode'];
$quizID = $_SESSION['quizID'];
include "includes/db.php";

$queries = '';
if(isset($_POST['submit'])){
    if ($quizType == 'c' || $quizType == 'd'){
        $correct_answers = $db->prepare("SELECT question, choice, points FROM qdetails JOIN question ON qdetails.question = question.questionID WHERE qTypeID = ? AND quiz = ?");
    }else {
        $correct_answers = $db->prepare("SELECT question, choice, points FROM qdetails JOIN question ON qdetails.question = question.questionID WHERE qTypeID = ? AND quiz = ? AND  statusMark = 'correct'");
    }
    $correct_answers->bind_param('si', $quizType, $quizID);
    $correct_answers->execute();
    $correct_answers_array = array();
    $i = 0;
    $a =0;
    $score = 0;
    $result = $correct_answers->get_result();
    $status = 'ok';

    foreach($_POST['option'] as $option_num => $option_val) {
        $option_trim = trim($option_val);
        if (empty($option_trim)) {
            $status = 'error';
            echo "<script>alert('No empty answers allowed.');
                        window.history.back()</script>";
        } else {
            $answer = $option_val;
            $row = $result->fetch_assoc();
            $qiDString = $row["question"];
            $correctAnswer = $row["choice"];
            if (strtolower($correctAnswer) == strtolower($answer)) {
                $score = $score + $row['points'];
                $setStatus = "correct";
                echo $studentID . " " . $qiDString . " " . $answer. " " . $quizType. " " . $setStatus . "<br>";
            } else {
                $setStatus = "incorrect";
                echo $setStatus . "<br>";
            }
            $queries .= "INSERT INTO studentquizlogs (idNum, queID, answers, quizTID, statusM) VALUES ('$studentID', '$qiDString', '$answer', '$quizType', '$setStatus'); ";
        }
    }
    if ($status == 'ok') {
        $queries .= "INSERT INTO studentattempt (attemptID, studentID, quizTypeID, quizCode, quizID, score) VALUES ('0', '$studentID', '$quizType','$quizCode','$quizID','$score');";
        $numQues =count(array($option_num));
        $result = mysqli_multi_query($db, $queries);
    }
}

if ($result) {
    do {
        // grab the result of the next query
        if (($result = mysqli_store_result($db)) === false && mysqli_error($db) != '') {
            echo "Query failed: " . mysqli_error($db);
        }

    } while (mysqli_more_results($db) && mysqli_next_result($db)); // while there are more results

} else {
    echo "First query failed..." . mysqli_error($db);
}
echo "<script>window.alert ('Successfully submitted quiz!');
              window.location.href = 'student_quiz.php?quizCode=$quizCode';</script>";
?>