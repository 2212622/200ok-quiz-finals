<?php
/**
 * Enter Code Page
 * Requires a student to enter a password to access the quiz.
 */
if(!isset($_SESSION))
{
    session_start();
}
include "includes/db.php";
$quizID = $_GET['qid'];
$_SESSION['quizID'] = $quizID;
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="style/questions.css">
    <script src="check-session.js"></script>
</head>

<body>
<div class="container">
    <div class="quiz-container">
        <div class='quiz-header'>
            <h2>200 OK</h2>
        </div>
        <form action="get_quiz.php" method="GET">
            <div class='quiz-container'>
                <ul>
                    <li>
                        <input type='input' class='form__field' placeholder='Enter Code' name='code' id='name' value='' pattern='^[A-Za-z0-9]+$'  onblur='this.value=removeSpaces(this.value);' required/>
                    </li>
                </ul>
            </div>
            <input type="submit" name="submit" value="Go" class="button btn3">
        </form>
    </div>
</div>
</body>
</html>