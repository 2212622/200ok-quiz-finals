/*
Referenced code for the homepage: https://github.com/sefyudem/Sliding-Sign-In-Sign-Up-Form
Modified by: Mendoza, Ivanka Chaental Louise
*/

const container = document.querySelector(".container");
const showPasswordStudent = document.querySelector("#show-password-student");
const passwordFieldStudent = document.querySelector("#password-student");

showPasswordStudent.addEventListener("click", function(){
    this.classList.toggle("fa-eye-slash");
    const type = passwordFieldStudent.getAttribute("type")
    === "password" ? "text" : "password";
    passwordFieldStudent.setAttribute("type", type);
});
