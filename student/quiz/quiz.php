<html>
<head>
    <title>200 OK Quiz</title>
    <link rel="stylesheet" href="../style/questions.css">
    <script src="../check-session.js"></script>
</head>
<body>

<?php
/**
 * Quiz Page
 * Displays the questions and input type
 * associated with the quiz type.
 */
if(!isset($_SESSION))
{
    session_start();
}



if(isset($_SESSION['status']))
{
    echo "<h4>".$_SESSION['status']. "</h4>";
    unset($_SESSION['status']);
}
$quizType = $_GET['type'];
$quizCode = $_SESSION['quizCode'];
$quizID = $_SESSION['quizID'];
$studentID = $_SESSION['studentID'];
include '../Quiz.php';
$db = new Quiz();
$questions = $db->get_questions($quizCode, $quizType);
$options = '';

$checkAttempt = $db->check_Attempt($studentID, $quizType, $quizID);

if($checkAttempt) {
    echo "<script> alert ('You have already answered this quiz'); window.history.back();</script>";
}
?>

<div class="container">
    <div class="quiz-container">
        <div class='quiz-header'>
        <?php switch ($quizType) {
            case "a":
                echo "<h2>MULTIPLE CHOICE</h2>";
                echo "</div><ul><li><p>Please choose the answer from the 
                        choices below and answer all the questions.</p></li></ul>";
                break;
            case "b":
                echo "<h2>TRUE OR FALSE</h2>";
                echo "</div><ul><li><p>Please choose the answer from the 
                        choices below and answer all the questions.</p></li></ul>";
                break;
            case "c":
                echo "<h2>IDENTIFICATION</h2>";
                echo "</div><ul><li><p>Please fill in the details 
                        and answer all the questions.</p></li></ul>";
                break;
            case "d":
                echo "<h2>FILL IN THE BLANKS</h2>";
                echo "</div><ul><li><p>Please fill in the details 
                        and answer all the questions.</p></li></ul>";
                break;
            case "e":
                echo "<h2>MATCHING TYPE</h2>";
                echo "</div><ul><li><p>Please choose the answer from the 
                        choices below and answer all the questions.</p></li></ul>";
                break;
            default:
                echo "200 OK";
        }?>
    </div>
    <br>
    <form action="../student_log.php" method="post">
        <?php
        $i=1;

        while ($row = $questions-> fetch_assoc()){
            echo "<div class='quiz-container'>
                  <h2>".$row['questions']."</h2>";
            if(!isset($row['image']) || empty($row['image'])) {

            }
            else
            {
                echo '<img class="meme" src="data:image/jpeg;base64,'.base64_encode($row['image']).'"/>';
            }

            $options = $db->choiceID($row['questionID']);
            echo "<ul>";
            if($quizType == 'a' || $quizType == 'b' || $quizType == 'e') {

                while ($row = $options->fetch_assoc()) {
                    $choice = $row['choice'];
                    echo "<li>
                            <input type='radio' name='option[$i]' id='option[$i]' value='" . $row['choice'] . "' required> " . $row['choice'] . "
                        </input></li>";
                }
            } else if ($quizType == 'c' || $quizType == 'd') {
                while($row=$options->fetch_assoc()){
                    echo "<li>
                           <input type='input' class='form__field' placeholder='Your answer' name='option[$i]' id='name' value='' pattern='^[A-Za-z \s*]+$' onblur='this.value=removeSpaces(this.value);' required />
                        </li>";
                }
            }
            echo "</ul>";
            echo "</div>";
            echo "<br>";
            $i++;
        }

        $_SESSION['options'] = $options;
        $_SESSION['quizType'] = $quizType;

        echo '<div class="button">
                <input type="submit" value="Submit" name="submit" class="btn"/>
            </div>'
        ?>
    </form>
</body>
</html>