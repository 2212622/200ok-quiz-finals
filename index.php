<?php
/**
 * Login Page
 * Authenticates student credentials.
 */
include ("student/includes/db.php");

$message = '';

if(isset($_POST["submit"])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if(empty($_POST["username"])) {
        $message = '<li>Username is required</li>';
        echo "<script>alert ('Username is required')</script>";
    } else if (preg_match('/\s/',$username)) { // Checks if there is white space
        $message = '<li>No spaces allowed</li>';
        echo "<script>alert ('No white spaces allowed')</script>";
    }

    if(empty($_POST['password'])) {
        $message = '<li>Password is required</li>';
        echo "<script>alert ('Password is required')</script>";
    } else if (preg_match('/\s/',$password)) { // Checks if there is white space
        $message = '<li>No spaces allowed</li>';
        echo "<script>alert ('No white spaces allowed')</script>";
    }

    if($message == '') {

        $statement = $db->prepare("SELECT * FROM students WHERE username = ?");
        $statement->bind_param('s', $username);
        $statement->execute();
        $result = $statement->get_result();

        if($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                if($row['password'] == $password) {
                    session_start();
                    session_regenerate_id();
                    $session_id = session_id();
                    $studentID = $row['studentID'];

                    $statement = $db->prepare("UPDATE students SET session_id = ? WHERE studentID = ?");
                    $statement->bind_param('ss', $session_id, $studentID);
                    $statement->execute();

                    $_SESSION['studentID'] = $row['studentID'];
                    $_SESSION['session_id'] = $session_id;
                    $_SESSION["username"] = $username;

                    header('location:student/home.php?q=1'); // Go to student homepage
                }
                else {
                    echo "<script>alert ('Invalid password!')</script>"; // Username exists but wrong password
                }
            }
        }
        else {
            echo "<script>alert ('Username does not exist!')</script>"; // Username does not exist
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>200 OK Quiz</title>
    <link rel="stylesheet" href="student/style/style.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"/>
</head>
<body>
<div class="container">
    <div class="forms-container">
        <div class="student-page">
            <form class="student-form" method="post">
                <img src="student/style/images/logo.png" class="quizlogo" alt="" />
                <h2 class="title">Student Login</h2>
                <div class="input-field">
                    <label>
                        <!-- Username Required and Maximum Length is 25 -->
                        <input type="text" placeholder="Username" name="username" maxlength="25" pattern='^[A-Za-z0-9]+$' required/>
                    </label>
                </div>
                <div class="input-field">
                    <label>
                        <!-- Password Required and Maximum Length is 16 -->
                        <input type="password" placeholder="Password" id="password-student" name="password" maxlength="16" pattern='^[A-Za-z0-9]+$' required/>
                        <i class="fa-solid fa-eye" id="show-password-student"></i>
                    </label>
                </div>
                <input id="play-btn" type="submit" value="Login" class="btn solid" name="submit"/>
            </form>
        </div>
    </div>
    <div class="panels-container">
        <div class="panel left-panel">
            <div class="content">
                <h3>Are you a teacher?</h3>
                <p> View the scores of each student. </p>
                <button class="btn transparent" id="teacher-btn" onclick="window.location.replace('http://200okquiz.com:12345');"> Click Here </button>
            </div>
            <img src="student/style/images/student1.png" class="image" alt="" />
        </div>
    </div>
</div>
<script src="student/script/home-page.js"></script>
</body>
</html>
