# 200 OK Quiz
An Online Quiz Management System built on PHP, Node.js, and JSP.

## Installation
1. Go to the `www` folder of your development environment.

2. Copy and paste the extracted `200ok` folder to the `www` folder.

3. Start the WAMP Server.

4. Open phpMyAdmin and create a new database named **200ok**.

5. Import the **200okquiz.sql** file into the newly created database.
#### If you are a Student:
1. Go to http://localhost/200ok/
2. Log in with an existing credentials

#### If you are a Teacher:
1. Install Node.js
2. Go to the `teacher` directory
3. In the terminal: 
    ```
    npm install
    npm install nodemon
    ```
4. Start the server by going to **package.json** and pressing the green play button before "start": "nodemon teacher.js"
5. Go to http://localhost:12345

#### If you are an Administrator:
1. The module requires Java 11 and above to work.
2. Install Apache Tomcat from https://tomcat.apache.org/download-10.cgi. Choose to download the installer.
2. Start the service. Press Windows key + R and type "services.msc". Look for Apache Tomcat.
3. Right-click the service and go to "Properties". Change the start-up type to "Manual" then start the service.
4. Go to the directory of Tomcat. Copy and paste "adminmodule.war" from the project directory.
5. Access localhost:8080/adminmodule in any browser.
